# frozen_string_literal: true

########################################################################################################################
# version: 0.0.1                                                                                                       #
# author: Jakub Kopriva                                                                                                #
# instructions: require 'jakub.rb'                                                                                     #
# instructions: Jakub.new.main(['dog_breed1', 'dog_breed2', ... ])                                                     #
########################################################################################################################
require 'json'
require 'psych'
require 'yaml'
require 'thread/pool'
require 'csv'
require 'net/http'

# class for coping with my doggies data ################################################################################
class Jakub
  # init + config file #################################################################################################
  def initialize
    filename = 'jakub.yml'
    begin
      @config = YAML.load_file("#{__dir__}/#{filename}")
    rescue Errno::ENOENT
      puts 'Problem loading config file'
      exit
    rescue Psych::SyntaxError
      puts 'Config file contains invalid YAML syntax'
      exit
    end
  end

  # json validity checker ##############################################################################################
  def validjson?(string)
    !JSON.parse(string) == false
  rescue JSON::ParserError
    false
  end

  # set up breed images URL ############################################################################################
  def images_url(breed)
    "#{@config['Endpoint']['BreedsImages']['URLprefix']}#{breed}#{@config['Endpoint']['BreedsImages']['URLsuffix']}"
  end

  # logger of first download which creates the file ####################################################################
  def log_download_create(source, filename)
    File.open(filename, 'a') do |file|
      file.sync = true
      file.flock(File::LOCK_EX)
      file.puts(JSON.pretty_generate({ "#{Time.now.strftime('%Y-%m-%d %H:%M:%S.%L')}": source.to_s }))
    end
  end

  # logger of downloads ################################################################################################
  def log_download(source, filename)
    json = []
    File.open(filename, 'r+') do |file|
      file.sync = true
      file.flock(File::LOCK_EX)
      json = JSON.parse(file.read)
      file.pos = 0
      file.puts(JSON.pretty_generate(json.merge!({ "#{Time.now.strftime('%Y-%m-%d %H:%M:%S.%L')}": source.to_s })))
    end
  end

  # json downloader ####################################################################################################
  def downloadjson(source, config)
    json = JSON.parse(Net::HTTP.get(URI(source)))
    validjson?(json.to_json)
    filename = "#{__dir__}/#{config['DownloadLog']['Name']}"
    if File.exist?(filename)
      log_download(source, filename)
    else
      log_download_create(source, filename)
    end
    puts "Fetching #{source} ... Json status: #{json['status']}"
    json
  end

  # check input ########################################################################################################
  # TODO: remove input duplicities?
  def input_validator(all_breeds)
    valid_arg = []
    @input_arr.each do |arg|
      valid_arg.append(arg) if all_breeds['message'].key?(arg)
    end
    if @input_arr.length != valid_arg.length
      puts "Excluding input argument(s): #{((@input_arr - valid_arg) | (valid_arg - @input_arr))}"
      puts "Continues on with these valid dog breeds only: #{valid_arg}"
    end
    valid_arg
  end

  # CSV files creation and filling #####################################################################################
  def csv_output(breed, all_breeds, images)
    CSV.open("#{__dir__}/#{breed}.csv", 'wb',
             write_headers: true, headers: ["sub-breeds of #{breed}:", "images of #{breed}:"]) do |csv|
      (0..[all_breeds['message'][breed].length, images['message'].length].max).each do |i|
        csv << [all_breeds['message'][breed][i], images['message'][i]]
      end
    end
  end

  def main(input_arr)
    @input_arr = input_arr
    # get and check json of all breeds #################################################################################
    all_breeds = downloadjson(@config['Endpoint']['ListAllBreeds']['URL'], @config)
    valid_arg = input_validator(all_breeds)

    # get data of valid input breed using thread pool ##################################################################
    # TODO: implement error handling
    pool = Thread.pool(@config['ThreadPool']['Size'])
    valid_arg.each do |a|
      pool.process { csv_output(a, all_breeds, downloadjson(images_url(a), @config)) }
    end
    pool.shutdown
  end
end
