# frozen_string_literal: true

require 'rspec/autorun'
require '/home/jakub/RubymineProjects/Jakub/jakub.rb'

describe Jakub do
  let(:jakub) { Jakub.new }

  it 'creates CSV file' do
    File.delete("#{__dir__}/husky.csv") if File.exist?("#{__dir__}/husky.csv")
    jakub.main(['husky'])
    expect(File.exist?("#{__dir__}/husky.csv")).to eq(true)
  end

  it 'updates download log file' do
    size_before = File.size("#{__dir__}/updated_at.json")
    jakub.main(['african'])
    expect(File.size("#{__dir__}/updated_at.json")).to be > size_before
  end

  it 'keeps log file in valid json format' do
    jakub.main(['terrier'])
    expect(jakub.validjson?(JSON.parse(File.open("#{__dir__}/updated_at.json", 'r').read.to_json))).to eq(true)
  end
end
